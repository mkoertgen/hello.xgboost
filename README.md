# hello.xgboost

Evaluating letter-recognition with [eXtreme Gradient Boosting (xgboost)](https://github.com/dmlc/xgboost)

Links:

- Original xgboost paper: [XGBoost: A Scalable Tree Boosting System](https://arxiv.org/abs/1603.02754) (Tianqi Chen, Carlos Guestrin, Mar/2016)
- [aws/sagemaker-python-sdk](https://github.com/aws/sagemaker-python-sdk)
- AWS SageMaker Developer Guide: https://docs.aws.amazon.com/sagemaker/latest/dg
- AWS SageMaker xgboost/MNIST example jupyter notebook: [Example: Hyperparameter Tuning Job](https://docs.aws.amazon.com/sagemaker/latest/dg/automatic-model-tuning-ex.html)

Testdata:

- http://deeplearning.net/data/mnist/mnist.pkl.gz
- http://archive.ics.uci.edu/ml/machine-learning-databases/letter-recognition/
